﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transformCluster : MonoBehaviour
{
    public float f,cx,cy;
    public float scaleX;
    public float scaleY;

    public GameObject[] planes = new GameObject[0];
    public string[] rotationStrings = new string[0];

    private bool start = false;

    public float truncationX = 0,truncationY = 0;
    public float fx = 1,fy = 1,fz = 1,fw = 1;
    public float ox = 90,oy = 0,oz = 0;
    public float zx = 1,zy = 1,zz = 1;
    public float fuu = 1;

    
    // Start is called before the first frame update
    void Update()
    {
        if(!start){return;}
        for (int i = 0; i < planes.GetLength(0); i++)
        {
        GameObject plane = planes[i];
        string[] ms = rotationStrings[i].Split(',');

        Matrix4x4 R = new Matrix4x4();

        R.SetColumn(0, new Vector4( float.Parse(ms[0]), float.Parse(ms[1]), float.Parse(ms[2]),0));
        R.SetColumn(1, new Vector4( float.Parse(ms[3]), float.Parse(ms[4]), float.Parse(ms[5]), 0));
        R.SetColumn(2, new Vector4( float.Parse(ms[6]), float.Parse(ms[7]), float.Parse(ms[8]), 0));
        R.SetColumn(3, new Vector4(0,0,0,1));
    
        float fu = f/100f;
        float cxu = cx/100f;
        float cyu = cy/100f;

        float icx = cxu*2;
        float icy = cyu*2;

         // Set Scale based on cx and cy. Consider Unitys scale factor of 10
        float scaleOffset = 0.1f*scaleX;
        
        float truncationXScaling = 1.0f-(truncationX*2);
        float truncationYScaling = 1.0f-(truncationY*2);
        
        float sizeX = icx*scaleOffset;
        float sizeY = icy*scaleOffset;

        float sizeXTrunc = sizeX*truncationXScaling;
        float sizeYTrunc = sizeY*truncationYScaling;

        plane.transform.localScale = new Vector3(
            sizeXTrunc*zx,
            zy,
            sizeYTrunc*zz
            );
        
        Quaternion rot = R.rotation;
        Quaternion changedRotation = new Quaternion(fx*rot.x,fy*rot.y,fz*rot.z,fw*rot.w);
        
        plane.transform.rotation = changedRotation * Quaternion.Euler(ox,oy,oz );
        plane.transform.position = changedRotation * new Vector3(0,0,1);
    
        
        plane.transform.position = plane.transform.position * fu * fuu;
        }
        
    }

    // Update is called once per frame
    void Start()
    {
        start = true;

        foreach (var plane in planes)
        {
        plane.transform.localScale = new Vector3(1, 1,1);
        plane.transform.rotation = Quaternion.Euler(0,0,0 );
        plane.transform.position = new Vector3(0,0,1);
            
        }
    }
}












/*
// ALLMOST WORKING
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transformCluster : MonoBehaviour
{
    public float f,cx,cy;
    public float scaleX;
    public float scaleY;

    public GameObject[] planes = new GameObject[0];
    public string[] rotationStrings = new string[0];

    private bool start = false;

    public float truncationX = 0,truncationY = 0;
    public float truncationXM = 1,truncationYM = 1;
    
    // Start is called before the first frame update
    void Update()
    {
        if(!start){return;}
        for (int i = 0; i < planes.GetLength(0); i++)
        {
        GameObject plane = planes[i];
        string[] ms = rotationStrings[i].Split(',');

        Matrix4x4 R = new Matrix4x4();

        R.SetColumn(0, new Vector4( float.Parse(ms[0]), float.Parse(ms[1]), float.Parse(ms[2]),0));
        R.SetColumn(1, new Vector4( float.Parse(ms[3]), float.Parse(ms[4]), float.Parse(ms[5]), 0));
        R.SetColumn(2, new Vector4( float.Parse(ms[6]), float.Parse(ms[7]), float.Parse(ms[8]), 0));
        R.SetColumn(3, new Vector4(0,0,0,1));
    
        float fu = f/100f;
        float cxu = cx/100f;
        float cyu = cy/100f;

        float icx = fu;
        float icy = (cyu/cxu)*fu;

         // Set Scale based on cx and cy. Consider Unitys scale factor of 10
        float scaleOffset = 0.1f*scaleX;
        
        float truncationXScaling = 1.0f-(truncationX*2);
        float truncationYScaling = 1.0f-(truncationY*2);
        
        float sizeX = icx*scaleOffset;
        float sizeY = icy*scaleOffset;

        float sizeXTrunc = sizeX*truncationXScaling;
        float sizeYTrunc = sizeY*truncationYScaling;

        plane.transform.localScale = new Vector3(
            sizeXTrunc,
            1,
            sizeYTrunc
            );
        
        Quaternion rot = R.rotation;
        Quaternion changedRotation = new Quaternion(rot.x,-rot.y,-rot.z,-rot.w);
        
        plane.transform.rotation = changedRotation * Quaternion.Euler(-90,0,0 );
        plane.transform.position = changedRotation * new Vector3(0,0,1);
    
        
        plane.transform.position = plane.transform.position * fu;
        }
        
    }

    // Update is called once per frame
    void Start()
    {
        start = true;

        foreach (var plane in planes)
        {
        plane.transform.localScale = new Vector3(1, 1,1);
        plane.transform.rotation = Quaternion.Euler(0,0,0 );
        plane.transform.position = new Vector3(0,0,1);
            
        }
    }
}




 */