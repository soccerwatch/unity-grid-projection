﻿Shader "Custom/Croping"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
        _BorderSize("_BorderSize", Vector) = (0.0,0.0,0.0,0.0)
        _CameraIndex ("CameraIndex", Vector) = (0.0,0.0,0.0,0.0)
		_ExtraRightBorder("RightGreenBordercorrection*1000",int) = 0.0049
	}
	SubShader
	{
		// No culling or depth
		ZWrite Off ZTest Off

		Pass
		{
			CGPROGRAM
            #pragma vertex vert 
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};


			sampler2D _MainTex;
			float4 _BorderSize;
            float2 _CameraIndex;
			float _ExtraRightBorder;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
                
				float width = (1.0-(_ExtraRightBorder))/ 2;
                float1 xStart = ((_CameraIndex.x) * width); //+ _BorderSize.x/2; 
                float1 yStart = (2-_CameraIndex.y) / 3; //+ _BorderSize.y/3;

                float1 x = (v.uv.x*width) + xStart;
                float1 y = (v.uv.y/3) + yStart;

                // apply cropping
                float localX = (v.uv.x*width);
                float localY = (v.uv.y/3);
                float localXCrop = localX*(1.0f - _BorderSize.z - _BorderSize.x) + _BorderSize.x*width;
                float localYCrop = localY*(1.0f - _BorderSize.w - _BorderSize.y) + _BorderSize.y/2;

                x = localXCrop + xStart;
                y = localYCrop + yStart;
                o.uv = float2(x,y);


				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				return tex2D(_MainTex, i.uv);
			}
			ENDCG
		}
	}
}